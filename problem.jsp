
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript" src="../js/jq-input-events.js" integrity="sha256-F0Wv3jjFugDkvQyAA2qBkoSCid67FUidH5O3g2QVwBs=" crossorigin="anonymous"></script>

<script type="text/javascript">

$(document).ready(function(){
	$("#periodSelectedId").datepicker({
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
	    buttonImage: "calendar.gif",
	    dateFormat: 'mmyy'
	}).focus(function() {
	    var thisCalendar = $(this);
	    $('.ui-datepicker-calendar').detach();
	    $('.ui-datepicker-close').click(function() {
	        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        thisCalendar.datepicker('setDate', new Date(year, month, 1));
	    });
	});
});


function exportReportIpg(){
	if($('#periodSelectedId').val() != null){
		$('#exportBillingReportButtonId').click();
	}else{
		alert('debe seleccionar el periodo ');
	}
}

</script>

<s:form id="billingReportForm" action="SearchBillingReport_export" namespace="/billing">
	<table class="ui-widget-content ui-corner-all tbFormCenter" style="width: 95%;">
		<tr>
			<th colspan="6" align="center"><h3><s:text name="billingReport.label.title" /></h3></th>
		</tr>
		
		<tr></tr>
 		<tr>
 		<td align="right" colspan="6">
			<table >
		 		<tr>
			 		<td>
						<s:text name="billingReport.label.date" />
						<font color="red"><s:text name="common.required" /></font>
					</td>
					<td>
						<sj:datepicker id="periodSelectedId"
										name="searchBillingReportDTO.periodSelectedId" 
										cssStyle="width: 60%;" />
						<img src="../struts/js/calendar.gif" >
					</td>
				</tr>
			</table>
		</td>
		</tr>
		<tr></tr>
		<tr>
		<td align="right" colspan="6">
				<table>
					<tr>
<!-- 						<td> -->
<%-- 							<security:authorize access="hasAuthority('ConsultaFact_Buscar')"> --%>
<%-- 								<s:url var="searchURL" action="SearchBillingReport_search" --%>
<%-- 									namespace="/billing" /> --%>
<%-- 								<sj:a id="searchBillingReportButtonId" --%>
<%-- 									formIds="billingReportForm" href="%{searchURL}" button="true" --%>
<%-- 									targets="bodyDivId"> --%>
<%-- 									<s:text name="button.search"></s:text> --%>
<%-- 								</sj:a> --%>
<%-- 							</security:authorize> --%>
<!-- 						</td> -->
						<td>
							<sj:a id="exportReportBillingId"
									onclick="exportReportIpg();" 
									button="true">
								<s:text name="button.export"></s:text>
							</sj:a>
							<sj:submit
								id="exportBillingReportButtonId"
								formIds="billingReportForm"
								cssStyle="display:none;"/>
						</td>
					</tr>					
				</table>
			</td>
		</tr>			    
			    
			    
	</table>
</s:form>
